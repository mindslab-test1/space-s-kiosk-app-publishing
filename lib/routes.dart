import 'package:get/get.dart';
import 'package:kiosk/home_screen.dart';
import 'package:kiosk/splash_screen.dart';

final List<GetPage> routes = [
  GetPage(name: "/SplashScreen", page: () => const SplashScreen()),
];
