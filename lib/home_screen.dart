import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kiosk/chatbot/choice_service_answer.dart';
import 'package:kiosk/setting_btn.dart';
import 'package:kiosk/setting_page.dart';
import 'package:usb_serial/transaction.dart';
import 'package:usb_serial/usb_serial.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:video_player/video_player.dart';
import 'package:flutter/services.dart';
import 'package:kiosk/time/current_time.dart';

import 'chatbot/choice_service.dart';

class HomeScreen extends StatefulWidget {
  VideoPlayerController videoController;

  HomeScreen({Key? key, required this.videoController}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // flutter display not 4k (ratio with the device connected)
  bool _isHello = false;
  late VideoPlayerController _videoController2;
  UsbPort? _port;
  String _status = "Idle";
  List<Widget> _ports = [];
  List<Widget> _serialData = [];

  StreamSubscription<String>? _subscription;
  Transaction<String>? _transaction;
  UsbDevice? _device;

  TextEditingController _textController = TextEditingController();

  Future<void> _connectSocket() async {
    var message = Uint8List(4);
    var bytedata = ByteData.view(message.buffer);

    bytedata.setUint8(0, 0x15);
    bytedata.setUint8(1, 0x00);
    bytedata.setUint8(2, 0x00);
    bytedata.setUint8(3, 0x00);

    print("SOCKET CONNECTING...");
    Socket socket = await Socket.connect('192.168.0.90', 9411); // status socket
    Socket socket2 = await Socket.connect('192.168.0.90', 9411); //vector
    print("SOCKET CONNECTED!");
    socket.add(message + utf8.encode("{\"cmd\": \"get_status\"}"));
    socket2.add(message + utf8.encode("{\"cmd\": \"get_vector\"}"));

    // send request
    // socket.add();
    // listen to the received data event stream
    print("LISTENING....");
    socket.listen((List<int> event) {
      // var tempOutput = Uint8List.fromList(event);
      // tempOutput.removeAt(0);
      String result = "";
      for (int i = 4; i < event.length; i++) {
        result = result + String.fromCharCode(event[i]);
      }

      // print(event);
      // print(utf8.decode(event));
      print(result);

      setState(() {
        _serialData.add(Text(result));
      });
    });
    socket2.listen((List<int> event) {
      // var tempOutput = Uint8List.fromList(event);
      // tempOutput.removeAt(0);
      String result = "";
      for (int i = 4; i < event.length; i++) {
        result = result + String.fromCharCode(event[i]);
      }

      // print(event);
      // print(utf8.decode(event));
      print(result);

      setState(() {
        _serialData.add(Text(result));
      });
    });
    return;
  }

  Future<bool> _connectTo(device) async {
    _serialData.clear();

    if (_subscription != null) {
      _subscription!.cancel();
      _subscription = null;
    }

    if (_transaction != null) {
      _transaction!.dispose();
      _transaction = null;
    }

    if (_port != null) {
      _port!.close();
      _port = null;
    }

    if (device == null) {
      _device = null;
      setState(() {
        _status = "Disconnected";
      });
      return true;
    }

    _port = await device.create();
    if (await (_port!.open()) != true) {
      setState(() {
        _status = "Failed to open port";
      });
      return false;
    }
    _device = device;

    await _port!.setDTR(true);
    await _port!.setRTS(true);
    //TODO Change PORT HERE
    await _port!.setPortParameters(
        9600, UsbPort.DATABITS_8, UsbPort.STOPBITS_1, UsbPort.PARITY_NONE);

    _transaction = Transaction.stringTerminated(
        _port!.inputStream as Stream<Uint8List>, Uint8List.fromList([13, 10]));

    _subscription = _transaction!.stream.listen((String line) {
      print("THERE IS MOTION!");
      print(line);
      // sensor recognized
      if (line == "yes") {
        setState(() {
          print("isHello TRUE");
          _isHello = true;
        });
        Timer(
            const Duration(seconds: 5),
            () => {
                  setState(() {
                    print("isHello FALSE");
                    _isHello = false;
                  })
                });
        //TODO dispose here
        // widget.videoController.dispose();
        // _videoController2.initialize().then((value) => {
        //       _videoController2.addListener(_checkVideo),
        //       _videoController2.play(),
        //       setState(() {
        //         _isHello = true;
        //       }),
        //     });

        print("result is true");
      }
      //TODO CHECK INCOMING LINE HERE
      setState(() {
        _serialData.add(Text(line));
        if (_serialData.length > 20) {
          _serialData.removeAt(0);
        }
      });
    });

    setState(() {
      _status = "Connected";
    });
    return true;
  }

  void _getPorts() async {
    _ports = [];
    List<UsbDevice> devices = await UsbSerial.listDevices();
    if (!devices.contains(_device)) {
      _connectTo(null);
    }
    print(devices);

    devices.forEach((device) {
      // * Connect Device if arduino
      if (device.productName == null) {
        _connectTo(_device == device ? null : device).then((res) {
          print("arduino connected");
        });
      }

      _ports.add(ListTile(
          leading: Icon(Icons.usb),
          title: Text(device.productName ?? "null"),
          subtitle: Text(device.manufacturerName!),
          trailing: ElevatedButton(
            child: Text(_device == device ? "Disconnect" : "Connect"),
            onPressed: () {
              _connectTo(_device == device ? null : device).then((res) {
                _getPorts();
              });
            },
          )));
    });

    setState(() {
      print(_ports);
    });
  }

  void _checkVideo() {
    print("check video");
    if (_videoController2.value.position == _videoController2.value.duration) {
      _videoController2.pause();
      _videoController2.dispose();
      widget.videoController.initialize().then((value) => {
            widget.videoController.setLooping(true),
            widget.videoController.play(),
            print("VIDEO 1 INTIALIZED"),
            _isHello = false,
            setState(() {}),
          });
      print('video Ended');
    }
  }

  void _initVideo() async {
    // _videoController2 = VideoPlayerController.asset("assets/video/hello.mp4",
    //     videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true));
    // await widget.videoController.dispose();
    // await _videoController2.initialize();
    // _videoController2.addListener(_checkVideo);
    // _videoController2.setLooping(true);
    // _videoController2.play();

    // ..initialize().then((value) => {
    //       _videoController2.addListener(_checkVideo),
    //       print("VIDEO 2 INTIALIZED"),
    //       setState(() {})
    //     });
  }

  @override
  void initState() {
    _getPorts();

    // _initVideo();
    super.initState();

    // * CONNECT SOCKET
    // _connectSocket();

    //? detect if there is plug in/ plug out of usb devices
    // UsbSerial.usbEventStream!.listen((UsbEvent event) {
    //   _getPorts();
    // });
  }

  @override
  void dispose() {
    super.dispose();
    _connectTo(null);
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    return MaterialApp(
      home: Scaffold(
        body: Center(
          child: Stack(
            children: [
              // wait video
              _isHello
                  ? Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: const Center(
                        child: Text("Include Video 2"),
                      ),
                    )
                  : SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: SizedBox.expand(
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: SizedBox(
                            width: widget.videoController.value.size.width,
                            height: widget.videoController.value.size.height,
                            //TODO change to videoController2
                            child: VideoPlayer(widget.videoController),
                          ),
                        ),
                      ),
                    ),
              Positioned(
                top: height * 0.02083,  // 80
                left: width * 0.03704,  // 80
                child: SizedBox(
                  height: height * 0.02765625,
                  width: width * 0.1490741,
                  child: SvgPicture.asset("assets/img/signatureLogo.svg"),
                ),
              ),
              Positioned(
                right: width * 0.03704,
                top: height * 0.0174479167,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    CurrentTime(),
                  ],
                ),
              ),
              ChoiceService(),
              // ChoiceServiceAnswer(),
              SettingBtn(),
            ],
          ),
        ),
      ),
    );
  }
}



// Column(children: [
//                 Text(
//                     _ports.length > 0
//                         ? "Available Serial Ports"
//                         : "No serial devices available",
//                     style: Theme.of(context).textTheme.headline6),
//                 ..._ports,
//                 Text('Status: $_status\n'),
//                 Text('info: ${_port.toString()}\n'),
//                 ListTile(
//                   title: TextField(
//                     controller: _textController,
//                     decoration: const InputDecoration(
//                       border: OutlineInputBorder(),
//                       labelText: 'Text To Send',
//                     ),
//                   ),
//                   trailing: ElevatedButton(
//                     child: Text("Send"),
//                     onPressed: _port == null
//                         ? null
//                         : () async {
//                             if (_port == null) {
//                               return;
//                             }
//                             String data = _textController.text + "\r\n";
//                             await _port!
//                                 .write(Uint8List.fromList(data.codeUnits));
//                             _textController.text = "";
//                           },
//                   ),
//                 ),
//                 Text("Result Data",
//                     style: Theme.of(context).textTheme.headline6),
//                 ..._serialData,
//               ]),