import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:ui';
import 'package:kiosk/chatbot/first_button.dart';

class ChoiceService extends StatefulWidget {
  const ChoiceService({Key? key}) : super(key: key);

  @override
  _ChoiceServiceState createState() => _ChoiceServiceState();
}

class _ChoiceServiceState extends State<ChoiceService> {
  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            // width: 760 / _widthRatio,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height * 0.2401,
                ),
                Container(
                  width: width * 0.4134259,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 0.3518519,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * 0.351852,
                              height: height * 0.0583,
                              child: Text(
                                '어떤일로 방문하셨나요? \n원하시는 서비스를 선택해주세요.',
                                style: TextStyle(
                                  fontFamily: 'notoSansKR',
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                  fontSize: width * 0.02593,// 56
                                  letterSpacing: -1.4,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * 0.00625, // 24
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height * 0.014167),// 40
                              child: GestureDetector(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(
                                      width * 0.007407), // 16
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00462963, // 10
                                      sigmaY: width * 0.00462963, // 10
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(
                                          width * 0.02963, // 64
                                          height * 0.0117875, // 48
                                          0,
                                          height * 0.0117875), // 48
                                      height: height * 0.04167,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width * 0.000462963, // 1
                                          color: const Color.fromRGBO(
                                              0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: width * 0.02963, // 64
                                            height: width * 0.02963, // 64
                                            child: SvgPicture.asset(
                                              "assets/img/ico_cube_64px.svg",
                                              color: Colors.white,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.02593),
                                          // 56
                                          Text(
                                            'SPACE-S 소개',
                                            style: TextStyle(
                                              fontFamily: 'naumSquare',
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white,
                                              fontSize: width * 0.02963,
                                              // 64
                                              height: 1.1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height * 0.014167),
                              child: GestureDetector(
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(width * 0.007407),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00462963,
                                      sigmaY: width * 0.00462963,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(
                                          width * 0.02963,
                                          height * 0.0117875,
                                          0,
                                          height * 0.0117875),
                                      height: height * 0.04167,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width * 0.000462963,
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: width * 0.02963,
                                            height: width * 0.02963,
                                            child: SvgPicture.asset(
                                              "assets/img/ico_info_64px.svg",
                                              color: Colors.white,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.02593),
                                          Text(
                                            '작업공간 안내',
                                            style: TextStyle(
                                              fontFamily: 'naumSquare',
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white,
                                              fontSize: width * 0.02963,
                                              height: 1.1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height * 0.014167),
                              child: GestureDetector(
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(width * 0.007407),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00462963,
                                      sigmaY: width * 0.00462963,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(
                                          width * 0.02963,
                                          height * 0.0117875,
                                          0,
                                          height * 0.0117875),
                                      height: height * 0.04167,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width * 0.000462963,
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: width * 0.02963,
                                            height: width * 0.02963,
                                            child: SvgPicture.asset(
                                              "assets/img/ico_mug_64px.svg",
                                              color: Colors.white,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.02593),
                                          Text(
                                            '개방형 협업 공간',
                                            style: TextStyle(
                                              fontFamily: 'naumSquare',
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white,
                                              fontSize: width * 0.02963,
                                              height: 1.1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height * 0.014167),
                              child: GestureDetector(
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(width * 0.007407),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00462963,
                                      sigmaY: width * 0.00462963,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(
                                          width * 0.02963,
                                          height * 0.0117875,
                                          0,
                                          height * 0.0117875),
                                      height: height * 0.04167,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width * 0.000462963,
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: width * 0.02963,
                                            height: width * 0.02963,
                                            child: SvgPicture.asset(
                                              "assets/img/ico_people_64px.svg",
                                              color: Colors.white,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.02593),
                                          Text(
                                            '멤버십 협업 공간',
                                            style: TextStyle(
                                              fontFamily: 'naumSquare',
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white,
                                              fontSize: width * 0.02963,
                                              height: 1.1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height * 0.014167),
                              child: GestureDetector(
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(width * 0.007407),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00462963,
                                      sigmaY: width * 0.00462963,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(
                                          width * 0.02963,
                                          height * 0.0117875,
                                          0,
                                          height * 0.0117875),
                                      height: height * 0.04167,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width * 0.000462963,
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: width * 0.02963,
                                            height: width * 0.02963,
                                            child: SvgPicture.asset(
                                              "assets/img/ico_car_64px.svg",
                                              color: Colors.white,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.02593),
                                          Text(
                                            '주차 안내',
                                            style: TextStyle(
                                              fontFamily: 'naumSquare',
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white,
                                              fontSize: width * 0.02963,
                                              height: 1.1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height * 0.014167),
                              child: GestureDetector(
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.circular(width * 0.007407),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00462963,
                                      sigmaY: width * 0.00462963,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(
                                          width * 0.02963,
                                          height * 0.0117875,
                                          0,
                                          height * 0.0117875),
                                      height: height * 0.04167,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width * 0.000462963,
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: width * 0.02963,
                                            height: width * 0.02963,
                                            child: SvgPicture.asset(
                                              "assets/img/ico_company_64px.svg",
                                              color: Colors.white,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.02593),
                                          Text(
                                            '입주 기업',
                                            style: TextStyle(
                                              fontFamily: 'naumSquare',
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white,
                                              fontSize: width * 0.02963,
                                              height: 1.1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: height * 0.014167),
                              child: GestureDetector(
                                child: ClipRRect(
                                  borderRadius:
                                  BorderRadius.circular(width * 0.007407),
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00462963,
                                      sigmaY: width * 0.00462963,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(
                                          width * 0.02963,
                                          height * 0.0117875,
                                          0,
                                          height * 0.0117875),
                                      height: height * 0.04167,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width * 0.000462963,
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                        MainAxisAlignment.start,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: width * 0.02963,
                                            height: width * 0.02963,
                                            child: SvgPicture.asset(
                                              "assets/img/ico_note_64px.svg",
                                              color: Colors.white,
                                            ),
                                          ),
                                          SizedBox(width: width * 0.02593),
                                          Text(
                                            '기타 정보',
                                            style: TextStyle(
                                              fontFamily: 'naumSquare',
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white,
                                              fontSize: width * 0.02963,
                                              height: 1.1,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            FirstButton(height: height, width: width),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
