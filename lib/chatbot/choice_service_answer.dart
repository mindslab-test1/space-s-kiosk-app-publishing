import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'dart:ui';
import 'package:kiosk/chatbot/first_button.dart';

class ChoiceServiceAnswer extends StatefulWidget {
  const ChoiceServiceAnswer({Key? key}) : super(key: key);

  @override
  _ChoiceServiceAnswerState createState() => _ChoiceServiceAnswerState();
}

class _ChoiceServiceAnswerState extends State<ChoiceServiceAnswer> {
  final double _heightRatio = 2.63;
  final double _widthRatio = 2.549;

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return Container(
      padding: EdgeInsets.only(right: width * 0.03704),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            // width: 760 / _widthRatio,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SizedBox(
                  height: height * 0.2401,
                ),
                SizedBox(
                  width: width * 0.4134259,
                  child: Stack(
                    children: [
                      SizedBox(
                        width: width * 0.4134259,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * 0.351852,
                              child: Text(
                                '안녕하세요. \nSPACE-S를 찾아주셔서 감사합니다. 무엇을 도와드릴까요?',
                                // '연구개발특구진흥재단이 운영하는 AI 창업교류공간 대덕특구 SPACE-S는 인공지능 기술 자원을 바탕으로 유망한 (예비)창업자를 육성하고 개방형 혁신문화를 만들어나가는 공간입니다.',
                                style: TextStyle(
                                  fontFamily: 'notoSansKR',
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                  fontSize: width * 0.02407, // 52
                                  letterSpacing: -1.3,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top:height * 0.01667),// 64
                              width: width*0.3518519, // 760
                              height: height*0.0625,  //240
                              constraints: BoxConstraints(
                                  minHeight: height*0.0625
                              ),
                              child: (
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(width * 0.007407), // 16
                                  child: BackdropFilter(
                                    filter: ImageFilter.blur(
                                      sigmaX: width * 0.00833, // 18
                                      sigmaY: width * 0.00833, // 00833
                                    ),
                                    child: Center(
                                      child: Container(
                                        padding: EdgeInsets.only(top:height*0.00781215, bottom:height*0.00781215),// 30
                                        width: width*0.3518519, // 760
                                        decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.4),
                                        ),
                                        child: Center(
                                            child: SizedBox(
                                              height: height*0.046875,
                                              child: Image.asset(
                                                "assets/img/img_ableai.png",
                                              ),
                                            )
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ),
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: height * 0.00625, // 24
                                ),
                                Container(
                                  width: width * 0.3518519, // 760
                                  margin: EdgeInsets.only(top: height * 0.014167), // 40,40
                                  child: Image.asset(
                                    "assets/img/chatbot_textImg.jpg",
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: height * 0.00625, // 24
                                ),
                                Wrap(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: height * 0.014167, right: width * 0.01852), // 40,40
                                      child: GestureDetector(
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.007407), // 16
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                              sigmaX: width * 0.00462963, // 10
                                              sigmaY: width * 0.00462963, // 10
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  width * 0.02222, // 48
                                                  height * 0.009375, // 36
                                                  width * 0.02222, // 48
                                                  height * 0.00833), // 32
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: width * 0.000462963, // 1
                                                  color: const Color.fromRGBO(
                                                      0, 0, 0, 0.25),
                                                ),
                                                color: Colors.black.withOpacity(0.4),
                                                boxShadow: const [
                                                  BoxShadow(
                                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                                    blurRadius: 24,
                                                    offset: Offset(12, 12),
                                                  )
                                                ],
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    'SPACE-S',
                                                    style: TextStyle(
                                                      fontFamily: 'naumSquare',
                                                      fontWeight: FontWeight.w400,
                                                      color: Colors.white,
                                                      fontSize: width * 0.02407, // 52
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: height * 0.014167, right: width * 0.01852), // 40,40
                                      child: GestureDetector(
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.007407), // 16
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                              sigmaX: width * 0.00462963, // 10
                                              sigmaY: width * 0.00462963, // 10
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  width * 0.02222, // 48
                                                  height * 0.009375, // 36
                                                  width * 0.02222, // 48
                                                  height * 0.00833), // 32
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: width * 0.000462963, // 1
                                                  color: const Color.fromRGBO(
                                                      0, 0, 0, 0.25),
                                                ),
                                                color: Colors.black.withOpacity(0.4),
                                                boxShadow: const [
                                                  BoxShadow(
                                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                                    blurRadius: 24,
                                                    offset: Offset(12, 12),
                                                  )
                                                ],
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '개방형 협업공간',
                                                    style: TextStyle(
                                                      fontFamily: 'naumSquare',
                                                      fontWeight: FontWeight.w400,
                                                      color: Colors.white,
                                                      fontSize: width * 0.02407, // 52
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: height * 0.014167, right: width * 0.01852), // 40,40
                                      child: GestureDetector(
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.007407), // 16
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                              sigmaX: width * 0.00462963, // 10
                                              sigmaY: width * 0.00462963, // 10
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  width * 0.02222, // 48
                                                  height * 0.009375, // 36
                                                  width * 0.02222, // 48
                                                  height * 0.00833), // 32
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: width * 0.000462963, // 1
                                                  color: const Color.fromRGBO(
                                                      0, 0, 0, 0.25),
                                                ),
                                                color: Colors.black.withOpacity(0.4),
                                                boxShadow: const [
                                                  BoxShadow(
                                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                                    blurRadius: 24,
                                                    offset: Offset(12, 12),
                                                  )
                                                ],
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '멤버십 협업공간',
                                                    style: TextStyle(
                                                      fontFamily: 'naumSquare',
                                                      fontWeight: FontWeight.w400,
                                                      color: Colors.white,
                                                      fontSize: width * 0.02407, // 52
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: height * 0.014167, right: width * 0.01852), // 40,40
                                      child: GestureDetector(
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.007407), // 16
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                              sigmaX: width * 0.00462963, // 10
                                              sigmaY: width * 0.00462963, // 10
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  width * 0.02222, // 48
                                                  height * 0.009375, // 36
                                                  width * 0.02222, // 48
                                                  height * 0.00833), // 32
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: width * 0.000462963, // 1
                                                  color: const Color.fromRGBO(
                                                      0, 0, 0, 0.25),
                                                ),
                                                color: Colors.black.withOpacity(0.4),
                                                boxShadow: const [
                                                  BoxShadow(
                                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                                    blurRadius: 24,
                                                    offset: Offset(12, 12),
                                                  )
                                                ],
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '기업 입주공간',
                                                    style: TextStyle(
                                                      fontFamily: 'naumSquare',
                                                      fontWeight: FontWeight.w400,
                                                      color: Colors.white,
                                                      fontSize: width * 0.02407, // 52
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: height * 0.014167, right: width * 0.01852), // 40,40
                                      child: GestureDetector(
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.007407), // 16
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                              sigmaX: width * 0.00462963, // 10
                                              sigmaY: width * 0.00462963, // 10
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  width * 0.02222, // 48
                                                  height * 0.009375, // 36
                                                  width * 0.02222, // 48
                                                  height * 0.00833), // 32
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: width * 0.000462963, // 1
                                                  color: const Color.fromRGBO(
                                                      0, 0, 0, 0.25),
                                                ),
                                                color: Colors.black.withOpacity(0.4),
                                                boxShadow: const [
                                                  BoxShadow(
                                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                                    blurRadius: 24,
                                                    offset: Offset(12, 12),
                                                  )
                                                ],
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '주차',
                                                    style: TextStyle(
                                                      fontFamily: 'naumSquare',
                                                      fontWeight: FontWeight.w400,
                                                      color: Colors.white,
                                                      fontSize: width * 0.02407, // 52
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: height * 0.014167, right: width * 0.01852), // 40,40
                                      child: GestureDetector(
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              width * 0.007407), // 16
                                          child: BackdropFilter(
                                            filter: ImageFilter.blur(
                                              sigmaX: width * 0.00462963, // 10
                                              sigmaY: width * 0.00462963, // 10
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  width * 0.02222, // 48
                                                  height * 0.009375, // 36
                                                  width * 0.02222, // 48
                                                  height * 0.00833), // 32
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: width * 0.000462963, // 1
                                                  color: const Color.fromRGBO(
                                                      0, 0, 0, 0.25),
                                                ),
                                                color: Colors.black.withOpacity(0.4),
                                                boxShadow: const [
                                                  BoxShadow(
                                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                                    blurRadius: 24,
                                                    offset: Offset(12, 12),
                                                  )
                                                ],
                                              ),
                                              child: Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    '기타정보',
                                                    style: TextStyle(
                                                      fontFamily: 'naumSquare',
                                                      fontWeight: FontWeight.w400,
                                                      color: Colors.white,
                                                      fontSize: width * 0.02407, // 52
                                                      height: 1,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            FirstButton(height: height, width: width)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
