import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:ui';
import 'first_button.dart';

class ChoiceServiceNone extends StatefulWidget {
  const ChoiceServiceNone({Key? key}) : super(key: key);

  @override
  _ChoiceServiceNoneState createState() => _ChoiceServiceNoneState();
}

class _ChoiceServiceNoneState extends State<ChoiceServiceNone> {
  final double _heightRatio = 2.63;
  final double _widthRatio = 2.549;

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    double width = mediaQuery.size.width;
    double height = mediaQuery.size.height;

    return  Container(
        padding: EdgeInsets.only(right:80/_widthRatio),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              // width: 760 / _widthRatio,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children:[
                  SizedBox(
                    height: 1005 / _heightRatio,
                  ),
                  Stack(
                    children: [
                      SizedBox(
                        width: width * 0.47315,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: width * 0.351852,
                              child: Text(
                                  '어떤일로 방문하셨나요? \n 원하시는 서비스를 선택해주세요.',
                                  style: TextStyle(
                                      fontFamily: 'notoSansKR',
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white,
                                      fontSize: width * 0.02593,
                                      letterSpacing: -1.4
                                  )
                              ),
                            ),
                            SizedBox(
                              height: height * 0.0268229167,
                            ),
                            GestureDetector(
                              child:ClipRRect(
                                borderRadius: BorderRadius.circular(16/_widthRatio),
                                child: BackdropFilter(
                                  filter: ImageFilter.blur(
                                    sigmaX: width*0.00463,
                                    sigmaY: width*0.00463,
                                  ),
                                  child: Container(
                                      padding: EdgeInsets.all(width*0.02963),
                                      height: height*0.127083,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: width*0.000462963,
                                          color: Color.fromRGBO(0, 0, 0, 0.25),
                                        ),
                                        color: Colors.black.withOpacity(0.4),
                                      ),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            height: width*0.07037,
                                            child: Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                SizedBox(
                                                    height: width*0.07037,
                                                    width: width*0.07037,
                                                    child: Image(
                                                      image: AssetImage("assets/img/ico_building_152px.png"),
                                                      width: width*0.07037,
                                                      fit: BoxFit.fitWidth,
                                                    )
                                                  // child: SvgPicture.asset("assets/img/ico_building_152px.svg"),
                                                ),
                                                SizedBox(
                                                  width: width*0.05185,
                                                  height: width*0.05185,
                                                  child: ClipRRect(
                                                    borderRadius: BorderRadius.circular(16/_widthRatio),
                                                    child: BackdropFilter(
                                                      filter: ImageFilter.blur(
                                                        sigmaX: width*0.00463,
                                                        sigmaY: width*0.00463,
                                                      ),
                                                      child: Center(
                                                        child: Container(
                                                          width: width*0.05185,
                                                          height: width*0.05185,
                                                          decoration: BoxDecoration(
                                                            shape: BoxShape.circle,
                                                            border: Border.all(
                                                              width: width*0.000462963,
                                                              color: Color.fromRGBO(0, 0, 0, 0.25),
                                                            ),
                                                            color: Colors.black.withOpacity(0.4),
                                                          ),
                                                          child: Center(
                                                              child: SizedBox(
                                                                width: width*0.0333,
                                                                child: SvgPicture.asset(
                                                                  "assets/img/ico_arrow_right_72px.svg",
                                                                  fit: BoxFit.fitWidth,
                                                                ),
                                                              )
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Text(
                                              'SPACE-S 안내',
                                              style: TextStyle(
                                                fontFamily: 'naumSquare',
                                                fontWeight: FontWeight.w400,
                                                color: Colors.white,
                                                fontSize: width * 0.02593,
                                              )
                                          ),
                                        ],
                                      )
                                  ),
                                ),
                              ),
                            ),
                            FirstButton(height: height, width: width)
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        )
    );
  }
}
